# https://www.tweag.io/blog/2020-05-25-flakes/
# Rename this to flake.nix...
{
	description = "Stig's sample flake";

	# The inputs attribute specifies other flakes that this flake depends on. These are fetched by Nix and passed as arguments to the outputs function.
	inputs = {
		# nixpkgs.url	= "github:NixOS/nixpkgs/nixos-24.11"; # why not fetch from unstable instead..
		nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
	};

	# The outputs attribute is the heart of the flake: it’s a function that produces an attribute set. The function arguments are the flakes specified in inputs.
	outputs = { self, nixpkgs }: {
		defaultPackage.x86_64-linux = 
			with import nixpkgs {system = "x86_64-linux";};
			stdenv.mkDerivation { 
				name = "hello"; 
				src = self; 
				buildPhase = "gcc -o hello.c";  # like the build() function in a PKGBUILD
				installPhase = "mkdir -p $out/bin && install -t $out/bin hello"; # PKGBUILD package()/install() function(s)
			};
	};
	# The self argument denotes this flake. Its primarily useful for referring to the source of the flake (as in src = self;) or to other outputs (e.g. self.defaultPackage.x86_64-linux) defined in this file.
	# The attributes produced by outputs can be (and usually are) arbitrary values - but there are some standard outputs such as defaultPackage.${system}. 
}

# Run nix build to build this flake
# Then `./result/bin/hello`
# which is the same as 
# `nix shell --command hello`
# which is also the same as
# `nix develop; eval "$buildPhase"; ./hello`
