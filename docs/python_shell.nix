with import <nixpkgs> {};

let 
  pyPackages = python3Packages;
in
pkgs.mkShell {
  name = "pyenv";
  venvDir = "./.venv";
  buildInputs = [  pyPackages.python pyPackages.venvShellHook pyPackages.numpy git];
  postVenvCreation = ''
    unset SOURCE_DATE_EPOCH
    # pip install -r requirements.txt
    '';
  postShellHook = ''
    echo "Wilkommen bei venv."
  '';
}
