# sample shell.nix file
# { pkgs ? import <nixpkgs> {} }:
with (import <nixpkgs> {});
mkShell {
  buildInputs = [
    pkgs.nano
    pkgs.git
  ];
  # shellHook-s run every time you create the environment
  shellHook = ''
    echo "Hello world"
  '';
}
