{
  description = "Stig's NixOS Flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    # nixos.url = "github:NixOS/nixos-hardware/master";
    home-manager = {
      url = "github:nix-community/home-manager/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nix-index-database = {
      url = "github:nix-community/nix-index-database";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    zen-browser = {
    	url = "github:0xc000022070/zen-browser-flake";
    	inputs.nixpkgs.follows = "nixpkgs";
    };
    agenix = {
      url = "github:ryantm/agenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    /*
    stylix = {
      url = "github:danth/stylix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    */
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, rust-overlay, home-manager, agenix, zen-browser, ... } @ inputs: {

    nixosConfigurations = {
      Krypton = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = inputs;
        modules = [
          
          ({ pkgs, ... }: {
            nixpkgs.overlays = [ rust-overlay.overlays.default ];
            environment.systemPackages = with pkgs; [
              zen-browser.packages."${system}".default
              agenix.packages.x86_64-linux.default
              gdb 
              lldb_19
              (rust-bin.stable.latest.default.override { extensions = ["rust-src" "rust-analyzer"];})
            ];
          })

          agenix.nixosModules.default
          {
            age = {
              identityPaths =  [ "/home/stig/.ssh/id_rsa" ];
              secretsDir= "/home/stig/.local/share/agenix/agenix";
              secretsMountPoint = "/home/stig/.local/share/agenix/agenix.d";
              # secretsDir= "/run/user/1000/agenix/agenix";
              # secretsMountPoint = "/run/user/1000/agenix/agenix.d";
              secrets = {
                git-credentials = {
                  file = ./hosts/Krypton/secrets/git-credentials.age;
                  mode = "777";
                  owner = "stig";
                  group = "wheel";
                };
              };
            };
          }
          # stylix.nixosModules.stylix
          # disko.nixosModules.disko
          ./hosts/Krypton/configuration.nix
          # Home Manager integration for Krypton
          home-manager.nixosModules.home-manager {
            home-manager = {
              backupFileExtension = "/tmp/${toString self.lastModified}.bak";
              useGlobalPkgs = true;
              useUserPackages = true;
              users.stig = {
                home.stateVersion = "25.05";
              };
            };
          }
        ];
      };

      vm = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = inputs;
        modules = [
          ./hosts/vm/configuration.nix
          # Home Manager integration for VM
          home-manager.nixosModules.home-manager {
            home-manager = {
              useGlobalPkgs = true;
              useUserPackages = true;
              users.stig = {
                home.stateVersion = "25.05";
                # home.packages = [ /* add packages */ ];
              };
            };
          }
        ];
      };
    };

    # Home Manager for user configuration
    homeConfigurations.stig = home-manager.lib.homeManagerConfiguration  {
        # configuration = hosts/Krypton/home.nix;
      modules = [ hosts/Krypton/home.nix ];
    };
  };
}

