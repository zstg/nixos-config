{ pkgs, ... }: {
  home.packages = with pkgs; [
    #mako libnotify
    # grc
    unzip # for nvim-Mason installer
    appimage-run
    # rustup
  ];
}
