# NixOS 23.11 Unstable configuration file
# Manually set channels to unstable (using scripts/unstable-nixos)
{
  config,
  stdenv,
  fetchFromGitHub,
  pkgs,
  inputs,
  ...
}: let
  dotFiles = "$USER/.dotfiles";
  binPath = "$HOME/.nix-profile/bin";
in {
  # Home manager only works on already existing users.
  # So we define users in a normal nixosModule (as opposed to a homeManagerModule).
  users.users.stig = {
    shell = pkgs.fish;
    isNormalUser = true;
    initialPassword = "test";
    description = "Stig";
    extraGroups = ["networkmanager" "wheel" "input" "video" "libvirtd" "dialout"]; # vboxusers docker
  };

  imports = [
    ../common/boot.nix
    ./programs/programs.nix
    ./services/services.nix
    # ./disko.nix
    ./hardware-configuration.nix # disko auto-partitions VMs, what do I do with this hardware-configuration file?
  ];

  environment.systemPackages = with pkgs; [
    # git # note that Git comes preinstalled on the live ISO
  ];

  # system.copySystemConfiguration = true;
  # system.autoUpgrade.enable = true;
  # system.autoUpgrade.allowReboot = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment? # don't set it here, flakes overrides this
  system.autoUpgrade.channel = "https://channels.nixos.org/nixos-unstable";
}
