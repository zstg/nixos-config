{pkgs,...}: {
  home-manager.users.stig = {
    home.packages = with pkgs;[ glib whitesur-kde banana-cursor]; # provides gsettings
    gtk = {
      enable = true;
      /*
      theme = {
      	package = pkgs.whitesur-gtk-theme; # pkgs.kdePackages.breeze-gtk;
      	name = "WhiteSur"; # "Breeze-GTK";
      };
      */
      # Disable here. Let Plasma handle this.
      /*
      theme = {
        package = pkgs.arc-theme;
        name = "Arc-Dark";
      };
      iconTheme = {
        package = pkgs.papirus-icon-theme;
        name = "ePapirus-Dark";
      };
      cursorTheme = {
        package = pkgs.bibata-cursors;
        name = "Bibata-Modern-Ice";
      };
      */
    };
  };
  /*
  qt = {
  	platformTheme = "kde";
  	style = "breeze";
  };
  */
}
