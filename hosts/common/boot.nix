{pkgs, ...}: {
  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    kernelParams = [ "fsck.mode=skip" "quiet" "loglevel=3"];
    kernelModules = [ "uinput" ];
    initrd = {
	checkJournalingFS = false;
	/*
	services.udev.rules.extraRules = ''
		SUBSYSTEM=="input", GROUP="uinput"
		KERNEL=="uinput", GROUP="uinput"
	'';
	*/
    };
    tmp.cleanOnBoot = true;
    loader = {
      systemd-boot.enable = true;
      timeout = 1;
    };
    binfmt.registrations.appimage = {
      wrapInterpreterInShell = false;
      interpreter = "${pkgs.appimage-run}/bin/appimage-run";
      recognitionType = "magic";
      offset = 0;
      mask = ''\xff\xff\xff\xff\x00\x00\x00\x00\xff\xff\xff'';
      magicOrExtension = ''\x7fELF....AI\x02'';
    };
  };
}
