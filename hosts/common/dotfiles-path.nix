{...}:
let 
  dotfilesPath = "$HOME/.dotfiles/scripts";
in 
{
  environment.etc."profile".text = ''
  export PATH=${dotfilesPath}:$PATH
  '';
}
