{...}: {
  imports = [
    ./programs/programs.nix
    ./services/services.nix
    ./boot.nix
    ./home.nix
    ./gtk.nix
    ./fonts.nix
    ./misc.nix
    ./locale.nix
    ./dotfiles-path.nix
    ./systemd.nix
    # ./stylix.nix
  ];
}
