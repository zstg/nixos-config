{...}:
{
  imports = [
    ./audio/audio.nix
    ./displaymanager/displaymanager.nix
    ./dbus/dbus.nix
    ./logind/logind.nix
    ./libinput/libinput.nix
    ./envfs/envfs.nix
    ./openssh/openssh.nix
    ./xdg/xdg.nix
    ./security/security.nix
    ./networking/networking.nix
  ];
}
