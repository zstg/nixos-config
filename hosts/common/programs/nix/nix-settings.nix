# https://nixos-and-flakes.thiscute.world/best-practices/nix-path-and-flake-registry
{lib,nixpkgs,...}: {
  nix = {
    registry.nixpkgs.flake = nixpkgs;
    channel.enable = false;
    settings = {
      sandbox = true;
      auto-optimise-store = true;
      trusted-users = [ "root" "stig" "@wheel" ];
      nix-path = lib.mkForce "nixpkgs=/etc/nix/inputs/nixpkgs";
      experimental-features = ["nix-command" "flakes"];
      # substituters = ["https://hyprland.cachix.org"];
      # trusted-public-keys = ["hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="];
    };
    optimise.automatic = true;
    gc.automatic = true;
    gc.options = "--delete-older-than 7d";
  };
  # NixPkgs settings
  /*
  nixpkgs.config = {
    allowUnfree = true;
    allowBroken = true;
  }
  */
  environment.etc."nix/inputs/nixpkgs".source = "${nixpkgs}";
}
