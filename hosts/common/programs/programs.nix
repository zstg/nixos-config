{...}: {
  imports = [
    ./starship/starship.nix
    ./shells/fish/fish.nix
    # ./shells/zsh/zsh.nix # fish better
    # ./kitty/kitty.nix # why use kitty when you can use ghostty
    ./gnupg/gnupg.nix
    ./ghostty/ghostty.nix
    ./nix/nix.nix
    ./git/git.nix
    ./ssh/ssh.nix
  ];
}
