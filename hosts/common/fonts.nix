{pkgs,...}: {
  fonts = {
    enableDefaultPackages = true;
    # packages = with pkgs; [ nerdfonts (nerdfonts.override { fonts = ["JetBrainsMono" "NerdFontsSymbolsOnly" ]; }}];
    packages = with pkgs.nerd-fonts; [ jetbrains-mono symbols-only ];
    fontconfig = {
      enable = true;
      hinting.enable = true;
   };
  };
}
