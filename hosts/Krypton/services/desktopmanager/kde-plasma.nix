{pkgs,...}:{
programs = {
    kdeconnect.enable = true;
  };
  home-manager.users.stig.home.packages = with pkgs; [
  	kdePackages.breeze-gtk
  	kdePackages.plasma-pa
  	kdePackages.plasma-nm
  	bluez bluez-tools kdePackages.bluedevil
  	
  ];
}
