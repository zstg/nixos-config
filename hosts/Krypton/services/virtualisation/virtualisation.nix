{pkgs,home-manager, ...}: {
  imports = [ 
	./docker/docker.nix 
	#./podman/podman.nix 
	./libvirtd/libvirtd.nix 
  ];
  home-manager.users.stig = {
    home.packages = [
      # pkgs.virt-manager
      pkgs.gnome-boxes
    ];
  };
}
