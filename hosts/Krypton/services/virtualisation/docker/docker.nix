{pkgs,...}:{
  services.dockerRegistry.enable = true;
  virtualisation = {
    docker = {
      enable = true;
      enableOnBoot = true;
      package = pkgs.docker;
      autoPrune.enable = true;
    };
  };
}
