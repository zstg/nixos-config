{ pkgs,...}:
let
  modifiedEmacs = import ../../programs/emacs/emacs.nix {pkgs = pkgs;}; 
in
{
  services = {
    emacs = {
      enable = true; 
      install = true;
      package = modifiedEmacs;
      defaultEditor = true;
    };
  };
  home-manager.users.stig.home.packages = with pkgs; [ emacsPackages.mu4e ]; # this is the equivalent of appmenu-gtk on Archlinux
}
