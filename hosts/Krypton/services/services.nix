{...}: {
  imports = [
    ../../common/services/services.nix
    ./flatpak/flatpak.nix
    ./fwupd/fwupd.nix
    # ./syncthing/syncthing.nix
    ./emacs/emacs.nix
    ./desktopmanager/desktopmanager.nix
    ./power-profiles-daemon/power-profiles-daemon.nix
    ./xserver/xserver.nix
    ./syncthing/syncthing.nix
    ./virtualisation/virtualisation.nix
    ./tailscale/tailscale.nix
    ./printing/printing.nix
  ];
  networking.hostName = "Krypton";
}
