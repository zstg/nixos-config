# note that stig and krypton are arbitrary, need not be the user and hostname resp.
let 
  # This key is the content of ~/.ssh/id_rsa.pub (since id_rsa is the private key)
  stig = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCzRMS8tY8AutiiwsM6jRw/J6fJLruZICFE+toifGihCT34mXZ8Iui9KP2QwPN/PCnClP5huWRDmNQ9t7rOxkZJYu5x8vf1dGvgN+njtk5HiJEu4NeaFKKYJ3r0rslZzVu1PrzHOIGgvM/2fJjLoA35xTGvy+lD8OzoA/HVojZ+Utn758oJG2TLjac+a53fLUQT1Bt2MQs8rEs42djqyJ0GLZBRDHM1GVXHSaO3WSjHck2JHDcyZ4a4L/PcFsQqrJJi9yOcMdGBnSiYB3UyHnjcOkAMtEQDf+mkK/Tj/9bvEpMx/UtBUFpsUUzJMlBYeL5gNv1/76P6FcaFu23Fw+vdNA+l+8wSyOUwc7gWWRoNz4e3aQMyQ1lDityCseHEXRwMBbLz0TPCLhngu7+97s7wd7+3G7/4sRJ7zj6A6b5LBF97yXzzssAS+jaDYWiMKUo7rWyUoxUNRFDo4rTiuXgf+J1Iiitw1bq7s8EzVUQE6ueM0TZJn99RjC2NzmKe8SY1tSBE8TEZsz3L/k/YDa2wFI+lumIpuEYhXweHEeYXfhQaRKuhZD4sIiwCSF/6SfctApvJs6nvynmm6llegYlbii/Q/q2g5/340It9S90C/B0e6EJWeponNbGPcyrJ+RYh7UXKI7JmFas9KEKT52IOeFXucxJ0ayvuCR9lLhSnNw== Add comment here";
  users = [ stig ];
  
  # krypton = stig; # obtained by running ssh-keyscan localhost
  # systems = [ krypton ];
in
{
  /*
  "secret1.age".publicKeys = [ stig krypton ];
  "secret2.age".publicKeys = users ++ systems; # this lets all configured users and systems access this secret. For now, I've restricted this to a single system, single user setup.
  */

  "git-credentials.age".publicKeys = stig; # krypton ];
}
