{ pkgs,... }:
# https://nixos.wiki/wiki/TexLive
let
  tex = (pkgs.texlive.combine {
    inherit (pkgs.texlive) scheme-basic
      dvisvgm dvipng # for preview and export as html
      etoolbox ec metafont
      wrapfig amsmath ulem hyperref capt-of;
      #(setq org-latex-compiler "lualatex")
      #(setq org-preview-latex-default-process 'dvisvgm)
  });
in  
{
  home-manager.users.stig.home.packages = with pkgs; [
    tex
    # zen.packages."${system}".generic
    zapzap
    polkit_gnome
    # clang-tools
    # gcc_latest # simply bcos every time i open nvim i get that mesg
    #mako libnotify
    micro
    brave
    pandoc
    ranger
    zoxide    
    python314FreeThreading pyright
    # tor-browser-bundle-bin
    distrobox
    # whatsapp-for-linux
    appimage-run
    # texliveSmall 
    libqalculate        
    # gnome.gnome-keyring
    # gnumake libtool cmake
    # nix-alien-pkgs.nix-alien
    nixd #nix-community's LSP
  ];
}
