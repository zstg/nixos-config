{pkgs,...}: 
let
  modifiedEmacs = pkgs.emacs30-pgtk.overrideAttrs (oldAttrs: {
    configureFlags = oldAttrs.configureFlags ++ [
      "--with-gnutls"
      "--without-xwidgets"
      "--with-tree-sitter"
      "--with-png"
      "-with-jpeg"
      "--without-wide-int"
      "-with-sound"
      # "--without-native-compilation"
      # "--with-pgtk" # means lucid won't work
      # "-with-x-toolkit=lucid"
      "--with-libsystemd"
      "-with-dbus"
      "--with-pdumper=yes"
      "--without-pop"
      "-without-mailutils"
    ];
  });
in
modifiedEmacs
