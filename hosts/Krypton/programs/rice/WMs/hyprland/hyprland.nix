{ pkgs,config, ... }: {
  # programs.hyprland.enable = true;
  wayland.windowManager.hyprland = {
    enable = false;
    xwayland = {
      enable = true;
    };
    systemd = {
      enable = true;
      enableXdgAutostart = true;
    };
    # Hyprland settings go here.
    /*
    settings = {
      # Monitor settings
      monitor = [
        { name = ""; resolution = "preferred"; scale = 1; }
        { name = "Virtual-1"; resolution = "1920x1080"; scale = 1; }
      ];

      # Environment variables
      env = {
        GTK_THEME = "Arc-Dark";
        GTK_ICON_THEME = "ePapirus-Dark";
        XCURSOR_SIZE = 24;
        HYPRCURSOR_SIZE = 24;
        BAT_THEME = "OneHalfDark";
        XDG_CURRENT_DESKTOP = "Hyprland";
        XDG_SESSION_TYPE = "wayland";
        XDG_SESSION_DESKTOP = "Hyprland";
        QT_QPA_PLATFORM = "wayland";
        GDK_BACKEND = "wayland,x11";
      };

      # Execute applications on startup
      exec-once = [
        "pypr"
        "hypridle"
        "${pkgs.bash}/bin/bash ~/.dotfiles/scripts/waybar-dark-v2"
        "dbus-update-activation-environment --systemd DISPLAY WAYLAND_DISPLAY XDG_CURRENT_DESKTOP"
        "swayosd-server"
        "albert"
        "${pkgs.bash}/bin/bash ~/.dotfiles/scripts/battery_checker"
        "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1"
        "gpg-agent --daemon"
        "emacs --daemon"
        "eww -c ~/.dotfiles/eww/dashboard/ daemon"
        "eww -c ~/.dotfiles/eww/bar/ daemon"
        "hyprpaper"
        "${pkgs.bash}/bin/bash ~/.dotfiles/scripts/eww-open-fancy"
        "wl-paste --primary --type text --watch cliphist store"
        "wl-paste --primary --type image --watch cliphist store"
        "wl-paste --type text --watch cliphist store"
        "wl-paste --type image --watch cliphist store"
        "nm-applet --indicator"
        "blueman-applet"
        "blueman-tray"
        "gsettings set org.gnome.desktop.interface gtk-theme 'Arc-Dark'"
        "gsettings set org.gnome.desktop.interface cursor-size 24"
        "gsettings set org.gnome.desktop.interface icon-theme 'Papirus-Dark'"
        "hyprpm reload -n"
      ];

      # Input settings
      input = {
        kb_layout = "us";
        kb_options = "compose:ralt";
        follow_mouse = true;

        touchpad = {
          tap-and-drag = true;
          disable_while_typing = false;
          natural_scroll = true;
        };

        sensitivity = 0.20;
        special_fallthrough = true;
      };

      # General settings
      general = {
        gaps_in = 1;
        gaps_out = 10;
        border_size = 3;
        resize_on_border = true;
        layout = "dwindle";

        windowrulev2 = [
          "float,size 20 20,class:^(Pcmanfm)$"
          "float,size 20 20,title:^(emacs-prompt)$"
          "float,size 20 20,class:^(emacs), title:^(*Minibuf-1* - GNU Emacs at Arch), initialclass:^(emacs)$, initialtitle:^(*dashboard* - GNU Emacs at Arch)$"
          "float,nofullscreenrequest,center,title:^(Archlinux-logout)$"
          "workspace 5,float,nofullscreenrequest,center,title:^(Spotify)$"
          "workspace 2,pseudotile,size 80 80,initialtitle:^(ONLYOFFICE Desktop Editors)$, initialClass:^(ONLYOFFICE Desktop Editors)$"
          "workspace 5 silent,pseudotile,nofullscreenrequest,center,initialtitle:^(WhatsApp Web)$"
          "workspace 5 silent,float,nofullscreenrequest,center,initialtitle:^(Tor Browser)$"
          "float,class:^(Rofi)$"
          "float,class:^(Conky)$"
          "opacity 0.95,class:^(kitty)$,title:^(NVIM)$"
          "workspace special,float,opacity 0.95,size 40 40,class:^(LibreWolf)$,title:^(LibreWolf — Sharing Indicator)$"
          "opacity 0.95,class:^(Emacs)$"
          "float,size  1300 800,nofullscreenrequest,center,class:^(.supertuxkart-wrapped)$,title:^(SuperTuxKart),initialclass:^(.supertuxkart-wrapped),initialtitle:^(SuperTuxKart)"
        ];
      };

      # Miscellaneous settings
      misc = {
        vfr = true;
        enable_swallow = true;
        swallow_regex = "^(kitty)$";
        disable_splash_rendering = true;
        disable_hyprland_logo = true;
        animate_manual_resizes = true;
        mouse_move_enables_dpms = true;
      };

      # Decoration settings
      decoration = {
        rounding = 7;

        blur = {
          enabled = false;
          size = 2;
          passes = 1;
          new_optimizations = true;
        };

        shadow = {
          enabled = true;
          range = 4;
          render_power = 3;
          color = "rgba(1a1a1aee)";
        };
      };

      # Animation settings
      animations = {
        enabled = true;
        bezier = [
          "overshot,0.05,0.9,0.1,1.05"
          "smoothOut,0.05,0,0.66,0.95"
          "smoothIn,0.05,1,0.5,1"
          "weird,-0.99,0,0.19,1"
        ];
        animation = [
          "workspaces,1,3,smoothIn"
          "windows,1,3.0,overshot,slide"
          "windowsOut,1,3.5,weird,slide"
          "windowsMove,1,2,default"
          "border,1,6,default"
          "fade,1,10,smoothIn"
          "fadeDim,1,10,smoothIn"
        ];
      };

      # Dwindle layout settings
      dwindle = {
        pseudotile = true;
        preserve_split = true;
      };

      # Master layout settings
      master = {
        new_status = "master";
      };

      # Gestures settings
      gestures = {
        workspace_swipe = "on";
        workspace_swipe_min_speed_to_force = 1;
        workspace_swipe_forever = false;
        workspace_swipe_invert = true;
      };

      mainMod = "SUPER";
      bindl = [
        ", switch:on:Lid Switch, exec, ~/.dotfiles/scripts/lidclose";
        ", switch:off:Lid Switch, exec, ~/.dotfiles/scripts/lidopen";
      ];
      bind = [
        "$mainMod, Q, killactive"; 
        "$mainMod SHIFT, B, exec, brave"; 
        "$mainMod CTRL, B, exec, brave --tor --profile-directory='Incognito'"; 
        "$mainMod, T, exec, pypr toggle term"; 
        "$mainMod, Return, exec, kitty"; 
        "$mainMod, L, exec, hyprlock"; 
        "$mainMod, SPACE, exec, albert toggle"; 
        "$mainMod, left, movefocus, l"; 
        "$mainMod, right, movefocus, r"; 
        "$mainMod, 1, workspace, 1"; 
        "$mainMod SHIFT, 1, movetoworkspace, 1";
      ];
      bindle = [
        # Additional keybinds
        "$mainMod SHIFT, R, exec, pkill ags && ags"; 
        "$mainMod SHIFT, E, exec, ~/.dotfiles/scripts/emacs-edit /home/stig/.config/hypr/hyprland.conf"; 
        "$mainMod, Print, exec, ~/.dotfiles/scripts/screenshotty"; 
        "$mainMod SHIFT, S, exec, ~/.dotfiles/scripts/screenshotty"; 
        "$mainMod, KP_Enter, exec, kitty -e zsh"; 
        "$mainMod SHIFT, W, exec, ~/.dotfiles/scripts/restart-eww"; 
        "$mainMod, W, exec, ~/.dotfiles/scripts/eww-open-fancy"; 
        "$mainMod, C, exec, ~/.dotfiles/scripts/configedit"; 
        "$mainMod, S, exec, ~/.dotfiles/scripts/screenshotty"; 
        "$mainMod, A, exec, ags -t quicksettings"; 
        "$mainMod, N, exec, ags -t notificationsmenu && sleep 1.5 && ags -t notificationsmenu"; 
        "$mainMod, X, exec, ags -t powermenu"; 
        "$mainMod, M, exec, ags -t mediamenu && sleep 2 && ags -t mediamenu"; 
        "$mainMod, E, exec, pypr toggle files"; 
        "$mainMod, L, exec, hyprlock"; 
        "$mainMod SHIFT, L, exit"; 
        "$mainMod, F, fullscreen"; 
        "$mainMod SHIFT, F, togglefloating"; 
        "$mainMod, V, exec, nwg-clipman"; 
        "$mainMod SHIFT, P, pseudo"; 
        "$mainMod, K, exec, ~/.dotfiles/scripts/stig-passmenu"; 
        "$mainMod, P, exec, keepassxc"; 
        "$mainMod, J, togglesplit"; 
        "$mainMod SHIFT, T, exec, ~/.dotfiles/scripts/hyprlayout"; 
        "$mainMod CTRL SHIFT, T, exec, tor-browser"; 
        "$mainMod, SPACE, exec, albert toggle"; 
        "$mainMod, D, exec, [float] ~/.dotfiles/scripts/emc -cF \"((visibility . nil))\" -e \"(emacs-run-desktop)\""; 
        "$mainMod SHIFT, E, exec, [float] ~/.dotfiles/scripts/emc -cF \"((visibility . nil))\" -e \"(emacs-run-symbols)\""; 
        "$mainMod, R, exec, [float] ~/.dotfiles/scripts/emc -cF \"((visibility . nil))\" -e \"(emacs-run-launcher)\""; 
        "$mainMod, Z, exec, ~/.dotfiles/scripts/emc"; 
        "$mainMod CTRL, Z, exec, ~/.dotfiles/scripts/restart-emacs"; 
        "$mainMod SHIFT, Z, exec, ~/.dotfiles/scripts/emd"; 
        "$mainMod, G, togglegroup"; 
        "$mainMod, Y, changegroupactive"; 
        "$mainMod, TAB, togglespecialworkspace"; 
        "$mainMod, left, movefocus, l"; 
        "$mainMod, right, movefocus, r"; 
        "$mainMod, up, movefocus, u"; 
        "$mainMod, down, movefocus, d"; 
        "$mainMod SHIFT, left, movewindow, l"; 
        "$mainMod SHIFT, right, movewindow, r"; 
        "$mainMod SHIFT, up, movewindow, u"; 
        "$mainMod SHIFT, down, movewindow, d"; 
        "$mainMod, 1, workspace, 1"; 
        "$mainMod, 2, workspace, 2"; 
        "$mainMod, 3, workspace, 3"; 
        "$mainMod, 4, workspace, 4"; 
        "$mainMod, 5, workspace, 5"; 
        "$mainMod, 6, workspace, 6"; 
        "$mainMod, 7, workspace, 7"; 
        "$mainMod, 8, workspace, 8"; 
        "$mainMod, 9, workspace, 9"; 
        "$mainMod, 0, workspace, 10"; 
        "$mainMod SHIFT, 1, movetoworkspace, 1"; 
        "$mainMod SHIFT, 2, movetoworkspace, 2"; 
        "$mainMod SHIFT, 3, movetoworkspace, 3"; 
        "$mainMod SHIFT, 4, movetoworkspace, 4"; 
        "$mainMod SHIFT, 5, movetoworkspace, 5"; 
        "$mainMod SHIFT, 6, movetoworkspace, 6"; 
        "$mainMod SHIFT, 7, movetoworkspace, 7"; 
        "$mainMod SHIFT, 8, movetoworkspace, 8"; 
        "$mainMod SHIFT, 9, movetoworkspace, 9"; 
        "$mainMod SHIFT, 0, movetoworkspace, 10"; 
        "$mainMod CTRL, 1, movetoworkspacesilent, 1"; 
        "$mainMod CTRL, 2, movetoworkspacesilent, 2"; 
        "$mainMod CTRL, 3, movetoworkspacesilent, 3"; 
        "$mainMod CTRL, 4, movetoworkspacesilent, 4"; 
        "$mainMod CTRL, 5, movetoworkspacesilent, 5"; 
        "$mainMod CTRL, 6, movetoworkspacesilent, 6"; 
        "$mainMod CTRL, 7, movetoworkspacesilent, 7"; 
        "$mainMod CTRL, 8, movetoworkspacesilent, 8"; 
        "$mainMod CTRL, 9, movetoworkspacesilent, 9"; 
        "$mainMod CTRL, 0, movetoworkspacesilent, 0"; 
        "$mainMod, mouse_down, workspace, e+1"; 
        "$mainMod, mouse_up, workspace, e-1"; 
        "$mainMod, mouse:272, movewindow"; 
        "$mainMod SHIFT, mouse:272, resizewindow"; 
        "ALT, mouse:272, resizewindow"; 
        "$mainMod, mouse:273, resizewindow"; 
        # Multimedia keys
        "XF86AudioRaiseVolume, exec, ~/.dotfiles/scripts/vol --up"; 
        "XF86AudioLowerVolume, exec, ~/.dotfiles/scripts/vol --down"; 
        "XF86MonBrightnessUp, exec, ~/.dotfiles/scripts/bri --up"; 
        "XF86MonBrightnessDown, exec, ~/.dotfiles/scripts/bri --down"; 
        "XF86AudioMute, exec, swayosd-client --output-volume mute-toggle"; 
        "XF86AudioPlay, exec, playerctl play-pause"; 
        "XF86AudioNext, exec, playerctl next"; 
        "XF86AudioPrev, exec, playerctl previous"; 
        # Misc bindings
        "CTRL, Escape, exec, kitty -e ~/.dotfiles/scripts/btop"; 
        "Caps_Lock, exec, swayosd-client --caps_lock"; 
      ];
      bind = [
        # Move focus with mainMod + arrow keys
        "$mainMod, left, movefocus, l"; 
        "$mainMod, right, movefocus, r"; 
        "$mainMod, up, movefocus, u"; 
        "$mainMod, down, movefocus, d"; 

        # Move window with mainMod + shift arrow keys
        "$mainMod SHIFT, left, movewindow, l"; 
        "$mainMod SHIFT, right, movewindow, r"; 
        "$mainMod SHIFT, up, movewindow, u"; 
        "$mainMod SHIFT, down, movewindow, d"; 

        # Switch workspaces with mainMod + [0-9]
        "$mainMod, 1, workspace, 1"; 
        "$mainMod, 2, workspace, 2"; 
        "$mainMod, 3, workspace, 3"; 
        "$mainMod, 4, workspace, 4"; 
        "$mainMod, 5, workspace, 5"; 
        "$mainMod, 6, workspace, 6"; 
        "$mainMod, 7, workspace, 7"; 
        "$mainMod, 8, workspace, 8"; 
        "$mainMod, 9, workspace, 9"; 
        "$mainMod, 0, workspace, 10"; 

        # Move active window to a workspace with mainMod + SHIFT + [0-9]
        "$mainMod SHIFT, 1, movetoworkspace, 1"; 
        "$mainMod SHIFT, 2, movetoworkspace, 2"; 
        "$mainMod SHIFT, 3, movetoworkspace, 3"; 
        "$mainMod SHIFT, 4, movetoworkspace, 4"; 
        "$mainMod SHIFT, 5, movetoworkspace, 5"; 
        "$mainMod SHIFT, 6, movetoworkspace, 6"; 
        "$mainMod SHIFT, 7, movetoworkspace, 7"; 
        "$mainMod SHIFT, 8, movetoworkspace, 8"; 
        "$mainMod SHIFT, 9, movetoworkspace, 9"; 
        "$mainMod SHIFT, 0, movetoworkspace, 10"; 

        # Move active window SILENTLY to a workspace with mainMod + CTRL + [0-9]
        "$mainMod CTRL, 1, movetoworkspacesilent, 1"; 
        "$mainMod CTRL, 2, movetoworkspacesilent, 2"; 
        "$mainMod CTRL, 3, movetoworkspacesilent, 3"; 
        "$mainMod CTRL, 4, movetoworkspacesilent, 4"; 
        "$mainMod CTRL, 5, movetoworkspacesilent, 5"; 
        "$mainMod CTRL, 6, movetoworkspacesilent, 6"; 
        "$mainMod CTRL, 7, movetoworkspacesilent, 7"; 
        "$mainMod CTRL, 8, movetoworkspacesilent, 8"; 
        "$mainMod CTRL, 9, movetoworkspacesilent, 9"; 
        "$mainMod CTRL, 0, movetoworkspacesilent, 0"; 

        # Scroll through existing workspaces with mainMod + scroll
        "$mainMod, mouse_down, workspace, e+1"; 
        "$mainMod, mouse_up, workspace, e-1"; 
      ];
      */
  };

  home.file = {
    ".config/hypr/hypridle.conf".text = builtins.readFile ./hypridle.conf;
    ".config/hypr/hyprland.conf".text = builtins.readFile ./hyprland.conf;
    ".config/hypr/hyprlock.conf".text = builtins.readFile ./hyprlock.conf;
    ".config/hypr/hyprpaper.conf".text = builtins.readFile ./hyprpaper.conf;
    ".config/hypr/pyprland.toml".text = builtins.readFile ./pyprland.toml;
  };
}
