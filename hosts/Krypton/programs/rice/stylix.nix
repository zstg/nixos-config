{pkgs,...}:{
stylix = {
  enable = true; # this is a must for some reason
  polarity = "dark";
  cursor = {
    package = pkgs.whitesur-cursors;
    name = "Whitesur cursors";
  };
  fonts = {
    /*
    monospace = {
      package = with pkgs.nerd-fonts; [ jetbrains-mono symbols-only ];
      name = "JetBrainsMono Nerd Font Mono";
    };
    */
  };
    /*
    baseScheme = {
      base00 = "282828";
    };
    */
    image = /home/stig/.dotfiles/wallpapers/black-hole-digital-art-space-art-4k-wallpaper-uhdpaper.com-187@5@b.jpg
  };
}
