{...}: {
  imports = [
    ../../common/common.nix
    ../home.nix
    ./nm-applet.nix
    ./dconf.nix
    ./light.nix
    # ./rice/rice.nix
  ];
  # systemd.user.startServices = "sd-switch";
}
