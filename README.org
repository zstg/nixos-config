#+Title: Setting up NixOS, the easy way!

:NOTE: This setup works better with systemd-boot, be sure to edit boot.nix and/or hardware-configuration.nix if you're setting up a GRUB-based install

* Initial live USB stuff

#+begin_src sh
nix-shell -p git --run "git clone --depth 1 https://gitlab.com/zstg/dotfiles ~/.dotfiles"
~/.dotfiles/nixos/unstable-nixos # not reqd if already on the unstable ISO
~/.dotfiles/nixos/setup-home-manager
#+end_src

** Disko
#+BEGIN_SRC sh
sudo nix run --experimental-features 'nix-command flakes' github:nix-community/disko/latest -- --mode destroy,format,mount ~/.nixos/hosts/vm/disko.nix
sudo nixos-install --root /mnt --flake .#vm
#+END_SRC

** On a VM

#+begin_src sh
  sudo parted /dev/vda -- mklabel gpt
  sudo parted /dev/vda -- mkpart esp FAT32 0% 1024MB
  # note that this formatting order matters!!
  sudo parted /dev/vda -- mkpart primary ext4 1024MB 100%
  sudo parted /dev/vda -- set 1 esp on

  sudo mkfs.ext4 -L nixos /dev/vda2
  sudo mkfs.fat -F32 -n boot /dev/vda1
#+end_src

** On hardware

#+begin_src sh
  sudo parted /dev/sda -- mklabel gpt 
  sudo parted /dev/sdb -- mklabel gpt 
  sudo parted /dev/sda -- mkpart primary ext4 0% 100% # /home 
  sudo parted /dev/sdb -- mkpart esp fat32 1mb 1024mb # /boot
  sudo parted /dev/sdb -- mkpart primary ext4 1024mb 100% # /
  sudo parted /dev/sdb -- set 1 esp on 

  sudo mkfs.ext4 -L home /dev/sda1 
  sudo mkfs.ext4 -L nixos /dev/sdb2 
  sudo mkfs.fat -F 32 -n boot /dev/sdb1
#+end_src

* Mount the formatted directories
:NOTE: Home mounting isn't compulsory
:NOTE: NO need to mess with the live USB's /etc/nixos, use /mnt/etc/nixos instead...

#+begin_src sh
sudo mount /dev/disk/by-label/nixos /mnt
sudo mkdir /mnt/boot # this is a must!
sudo mount /dev/disk/by-label/boot /mnt/boot
sudo nixos-generate-config --root /mnt
sudo cp /mnt/etc/nixos/hardware-configuration.nix ~/.dotfiles/nixos
#+end_src


* Symlink config files and install
Disko automatically handles partitions...
#+begin_src sh
sudo ln -sf ~/.dotfiles/nixos/**.nix /mnt/etc/nixos
sudo nixos-install
#+end_src


* NOTE
Use =show-trace= to get the error calling site.
#+BEGIN_SRC sh
nixos-rebuild build --flake ".#Krypton" --show-trace
#+END_SRC
